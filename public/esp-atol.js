<!-- eap-atol.js -->

class ATOL {
    constructor(login, password, URL='http://127.0.0.1:16732/api/v2') {
        this.login = login
        this.password = password
        this.URL = URL
    }

    /**
     * Returns Promise with JSON, which needs to be
     * resolved with .then() to get the result
     * https://stackoverflow.com/questions/14220321/how-to-return-the-response-from-an-asynchronous-call
     */
    POST(path, jsonstr = '') {
        return fetch(this.URL + path, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': 'Basic ' + btoa(this.login + ':' + this.password)
            },
            body: jsonstr
          })
          .then(response => {
              if (response.ok) {
                  return response.json();
              } else {
                  console.log('error: response is not 200');
                  throw new Error(response);
              }
          })
          .then(json => {
              // pretty print JSON with 2 indentation level
              var str = JSON.stringify(json, null, 2);
              console.log('ATOL JSON: ', str);
              return json;
          })
          .catch(e => {
              console.log("ATOL Error", e);
              return e;
          }
        );
    }

    GET(path, success, error) {
	fetch(this.URL + path, {
          headers: {
            'Authorization': 'Basic ' + btoa(this.login + ':' + this.password)
          }
        })
        .then(response => response.json())
        .then(json => success(json))
        .catch(e => error(e))
    }
}


    async function GetServerInfo() {
        fetch('http://127.0.0.1:16732/api/v2/serverInfo')
          .then(response => response.json())
          .then(json => {
              // pretty print JSON with 2 indentation level
              var str = JSON.stringify(json, null, 2);
              ServerInfo.value = str;
              Status.value = 'OK'
          })
          .catch(e => {
              ServerInfo.value = e;
              Status.value = 'Error'
          });
    }

    async function PrintReceipt() {
        fetch('http://127.0.0.1:16732/api/v2/requests', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: OrderData.value
          })
          .then(response => response.json())
          .then(json => {
              // pretty print JSON with 2 indentation level
              var str = JSON.stringify(json, null, 2);
              ServerInfo.value = str;
              Status.value = 'OK'
          })
          .catch(e => {
              ServerInfo.value = e;
              Status.value = 'Error'
          });
    }

    async function CheckStatus() {
        var uuid = JSON.parse(OrderData.value).uuid;
        var url = 'http://127.0.0.1:16732/api/v2/requests/' + uuid;
        fetch(url)
          .then(response => response.json())
          .then(json => {
              // pretty print JSON with 2 indentation level
              var str = JSON.stringify(json, null, 2);
              ServerInfo.value = str;
              Status.value = 'OK'
          })
          .catch(e => {
              ServerInfo.value = e;
              Status.value = 'Error'
          });
    }

<!-- /eap-atol.js -->

