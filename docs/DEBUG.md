Проверка что веб-сервер ATOL работает и локально доступен.

    curl -v "http://127.0.0.1:16732/api/v2/serverInfo"

Проверка, что у веб-сервера есть соединение с кассой (требует авторизацию).

    curl -v -u "user:password" "http://127.0.0.1:16732/api/v2/devices"

Хотя бы одно устройство из списка выше должно быть isActive, isDefault и hasLicense. Затем можно запросить информацию об этом устройстве по умолчанию.

    curl -v -X POST -u "user:password" "http://127.0.0.1:16732/api/v2/operations/queryDeviceInfo"

Настройки и прочие запросы в API https://app.swaggerhub.com/apis-docs/atol-dev/fptr-web-requests/1.0.0.0
