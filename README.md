Печать чеков для услуг на АТОЛ через JSON задания.

Контакты/заказ: https://easysoftware.pro/contact/

Демо/тест: https://abitrolly.gitlab.io/orders-atol/

[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/abitrolly/orders-atol)
[![CI/CD Status](https://img.shields.io/gitlab/pipeline-status/abitrolly/orders-atol?branch=master)](https://gitlab.com/abitrolly/orders-atol/-/pipelines)

---

Atol cash register integration in plain HTML5 using [Web Server API](
    https://app.swaggerhub.com/apis-docs/atol-dev/fptr-web-requests/1.0.0.0
).
